$(function() {
    let id_categoria=0;
    $("#table_existencias").dataTable().fnDestroy();
    llenar_Existencias(Event,id_categoria);
    llenar_categoria(Event);
});




 //FUNCION PARA LLENAR EL COMBO DE CATEGORIAS
 function llenar_categoria(e, id_categoria) {
    e.preventDefault;
    url = '/listar_categorias';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            if (data.length >= 1) {
                $('#categoria').empty();
                $('#categoria').append('<option value=0>Seleccione</option>');
                if (id_categoria === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#categoria').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === id_categoria) {
                            $('#categoria').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                           
                        } else {
                            $('#categoria').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                            
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}


$(document).on('submit', '#filtrar', function(e) {
    e.preventDefault();
    let id_categoria= $('#categoria').val();
    $("#table_existencias").dataTable().fnDestroy();
    llenar_Existencias(Event,id_categoria);

});

function llenar_Existencias(e,id_categoria) {
    var fechaActual = new Date();
    var dia = fechaActual.getDate();
    var mes = fechaActual.getMonth() + 1; // Los meses comienzan desde 0, por lo que se suma 1
    var año = fechaActual.getFullYear(); 
    // Formatear el día y el mes con dos dígitos
    if (dia < 10) {
      dia = "0" + dia;
    }
    if (mes < 10) {
      mes = "0" + mes;
    }
        // Paso 3: Genera el formato "dia mes año"
        var fecha = dia + "-" + mes + "-" + año;
   
    let ruta_imagen = rootpath;
    let encabezado = ''
    if(id_categoria!='0')
        {   
           
            if (id_categoria=='1') 
            {
                encabezado ='Categoria ='+' '+'Productos'
            }else if (id_categoria=='2') 
            {
                encabezado = 'Categoria ='+' '+'Bienes'
            }
        }
    
    var table = $('#table_existencias').DataTable({
        dom: "Bfrtip",
        buttons: {
            dom: {
                button: {
                    className: 'btn-xs-xs'
                },
            },
            buttons: [{
                    //definimos estilos del boton de pd
                    extend: "pdf",
                    text: 'PDF',
                    className: 'btn-xs btn-dark',
                    orientation: 'landscape',
                    pageSize: 'LETTER',
                    header: true,
                    footer: true,
                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4],
                    },
                    alignment: 'center',

                    customize: function(doc) {
                        //Remove the title created by datatTables
                        doc.content.splice(0, 1);
                        doc.styles.title = {
                            color: '#4c8aa0',
                            fontSize: '18',
                            alignment: 'center'
                        }
                        doc.styles['td:nth-child(2)'] = {
                                width: '130px',
                                'max-width': '130px'
                            },
                            doc.styles.tableHeader = {
                                fillColor: '#4c8aa0',
                                color: 'white',
                                alignment: 'center'
                            },
                            // Create a header
                            doc.pageMargins = [20, 95, 0, 70];
                        doc['header'] = (function(page, pages) {
                            doc.styles.title = {
                                color: '#4c8aa0',
                                fontSize: '18',
                                alignment: 'center',
                            }
                            return {
                                columns: [{
                                        margin: [10, 3, 40, 40],
                                         image: ruta_imagen,
                                        width: 780,
                                        height: 46,

                                    },
                                    {
                                        margin: [-800, 50, -25, 0],
                                        color: '#4c8aa0',
                                        fontSize: '18',
                                        alignment: 'center',
                                        text: 'Existencias al dia '+' '+fecha,
                                        fontSize: 18,
                                    },
                                    {
                                        margin: [-600, 80, -25, 0],
                                        text: encabezado,
                                    },
                                ],
                            }
                        });
                        // Create a footer
                        doc['footer'] = (function(page, pages) {
                            return {
                                columns: [{
                                    alignment: 'center',
                                    text: ['pagina ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                }],
                            }
                        });

                    },
                },

                {
                    //definimos estilos del boton de excel
                    extend: "excel",
                    text: 'Excel',
                    className: 'btn-xs btn-dark',
                    title: 'ExistenciasAlDia'+fecha,

                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4],
                    },
                    excelStyles: {
                        "template": [
                            "blue_medium",
                            "header_blue",
                            "title_medium"
                        ]
                    },

                }
            ]
        },
        "order": [
            [0, "asc"]
        ],
        "paging": true,
        "lengthChange": true,

        dom: 'Blfrtip',
        "searching": true,
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Todos']
        ],
        "ordering": true,
        "info": true,
        "autoWidth": true,
        //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
        "ajax": {
            "url": "/listar_existencias/"+id_categoria,
            "type": "GET",
            dataSrc: ''
        },
        "columns": [
            //a.usupnom,a.ususnom,a.usupape,a.ususape,a.usuemail,b.depnom


            { data: 'codbar' },
            { data: 'itemid' },
            { data: 'prodmar' },
           { data: 'prodmodel' },
            { data: 'numexis' },


        ],
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": [{
                "targets": [0],
                "visible": false,
                "searchable": false
            }, ]

        },
    });
}