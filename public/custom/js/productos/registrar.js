let borrado = $("#borrado_actual").val();
let id_categoria = $("#id_categoria").val();
llenar_categoria(Event,id_categoria);



 //FUNCION PARA LLENAR EL COMBO DE CATEGORIAS
 function llenar_categoria(e, id_categoria) {
    e.preventDefault;
    url = '/listar_categorias';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            if (data.length >= 1) {
                $('#categoria').empty();
                $('#categoria').append('<option value=0>Seleccione</option>');
                if (id_categoria === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#categoria').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === id_categoria) {
                            $('#categoria').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                           
                        } else {
                            $('#categoria').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                            
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}





if (borrado == '0') {
    $("#borrado").prop("checked", true);
} else {
    $("#borrado").prop("checked", false);
}

//******METODO QUE TOMA EL ESTATUS ACTUAL DEL CHECKBO*****
$('#borrado').click(function() {
    if ($('#borrado').is(':checked')) {

        $("#borrado_actual").val('0');
    } else {
        $("#borrado_actual").val('1');

    }

});

$(document).on('submit', '#nuevoproducto', function(e) {
    e.preventDefault();
    let buscar_codbar = $("#codbar").val();
    let borrado = $("#borrado_actual").val();
    let id_categoria = $("#categoria").val();
   if (id_categoria=='0') 
    {
        alert('Debe Seleccionar una Categoria');
    }else
    {
       var form = {
        'modform': $("#modform").val(),
        'codbar': $("#codbar").val(),
        'prodmar': $("#prodmar").val(),
        'prodmodel': $("#prodmodel").val(),
        'borrado': $("#borrado_actual").val(),
        'id_categoria': $("#categoria").val()
    };
    //METODO PARA VERIFICAR QUE EL PRODUCTO NO TENGA EXISTENCIAS ANTES DE HACER EL BORRADO LOGICO 
    $.ajax({
        url: '/buscar_producto_existencias/' + buscar_codbar,
        method: 'POST',
        data: { 'data': btoa(JSON.stringify(form)) },
        dataType: 'JSON',
        beforeSend: function() {

        },
        success: function(data) {
            if (data == '') {
                //NUEVO PRODUCTO
                let buscar_codbar = $("#codbar").val();
                $.ajax({
                    url: '/addproduct',
                    method: 'POST',
                    data: { 'data': btoa(JSON.stringify(form)) },
                    dataType: 'JSON',
                    beforeSend: function() {
                        $("button[type=submit").attr('disabled', 'true');
                    },
                    success: function(response) {
                        window.location = '/consultaproducto'
                    },
                    error: function(request) {
                        Swal.fire('Error!', request.responseJSON.message, 'error');

                    }
                });

            } else if (data[0].numexis > 0 && borrado == 1) {

                //EL PRODUCTO YA EXISTE Y SE VA A ACTUALIZAR 
                alert('No se puede eliminar el producto debido a que tiene existencia en inventario')
            } else {

                $.ajax({
                    url: '/addproduct',
                    method: 'POST',
                    data: { 'data': btoa(JSON.stringify(form)) },
                    dataType: 'JSON',
                    beforeSend: function() {
                        $("button[type=submit").attr('disabled', 'true');
                    },
                    success: function(response) {
                        window.location = '/consultaproducto'
                    },
                    error: function(request) {
                        Swal.fire('Error!', request.responseJSON.message, 'error');

                    }
                });

            }

        }

    });

    }
    
});