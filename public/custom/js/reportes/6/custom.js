$('.select2').select2({
    theme:"bootstrapbs4",
});

//Evento para resetear el formulario
$("button[type=reset]").on('click', function(){
    $("#detalles").hide();
    $("#consulta-fecha")[0].reset();
});

//Evento para ocultar los proveedores para la salida
$("#tipo-consulta").on('change', function(){
    if($("#tipo-consulta").val() == "2"){
        $("#proveedor-consulta").attr('disabled', "true");
    }
    else{
        $("#proveedor-consulta").removeAttr('disabled');   
    }
});

llenar_categoria(Event);

 //FUNCION PARA LLENAR EL COMBO DE CATEGORIAS
 function llenar_categoria(e, id_categoria) {
    e.preventDefault;
    url = '/listar_categorias';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            if (data.length >= 1) {
                $('#categoria').empty();
                $('#categoria').append('<option value=0>Seleccione</option>');
                if (id_categoria === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#categoria').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === id_categoria) {
                            $('#categoria').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                           
                        } else {
                            $('#categoria').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                            
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}


//Evento para la consulta de reportes de entradas y salidas por rango de fecha
$("#rango-consulta").on('focus', function(e){
	e.preventDefault();
	$("#rango-consulta").daterangepicker({
		showDropdowns: true,
        maxDate: fecha(),
        locale: {
            format: 'DD/MM/YYYY',
            daysOfWeek: [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ]
        }
	});
});

/*Funcion que realiza la fecha de hoy*/
function fecha(){
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1;
    var yy = hoy.getFullYear();
    var fecha = '';
    if(dd<10){
        dd = '0'+dd;
    }
    else if(mm<10){
        mm = '0'+mm;
    }
    fecha = dd+"/"+mm+"/"+yy;
    return fecha;
}
//Funcion que invierte la fecha para la BD
function invertirFecha(fecha){
	let fectmp = fecha.split('/');
	let fechadb = `${fectmp[2]}-${fectmp[1]}-${fectmp[0]}`;
	return fechadb;
}

//Evento para el envio de la consulta por fecha
$("#consulta-fecha").on('submit', function(e){
	e.preventDefault();
	let desde                =$('#desde').val();
	let hasta                =$('#hasta').val();
    let id_categoria                =$('#categoria').val();
	if (desde=='') 
	{
		desde='null' 
	}
	 if (hasta=='') 
	{
		hasta='null' 
	}
	if (hasta=='null'&&desde=='null') 
	{
		alert('DEDE INDICAR UN RANGO DE FECHA');
	} 
	else if (desde=='null'&&hasta!='null') 
	{
		alert('DEDE INDICAR EL CAMPO DESDE');
		
	}
	else if (hasta=='null'&&desde!='null') 
	{
		alert('DEDE INDICAR EL CAMPO HASTA');
	} 
	else if (hasta<desde) {
	  alert('EL CAMPO DESDE ES MAYOR AL CAMPO HASTA')
	}else
    {
        let datos = {
            'modo'              : $("#tipo-consulta").val(),
            'fecha_inicio'      : desde,
            "fecha_fin"         : hasta,
            'usuario-consulta'  : $("#usuario-consulta").val(),
            'producto-consulta' : $("#producto-consulta").val(),
            'proveedor-consulta': $("#proveedor-consulta").val(),
        }
        $.ajax({
            url:"/consultaFecha/"+id_categoria,
            method:"POST",
            dataType:"JSON",
            data: {
                "data" : btoa(JSON.stringify(datos))
            },
            beforeSend: function(){
                $("button[type=submit]").attr("disabled", "true");
            }
        }).then(function(response){
            $("#detalles-consulta").html(atob(response.data.tabla));
            $("#generar-reporte").html(response.data.footer);
            $("#detalles").show();
        }).catch(function(request){
            Swal.fire("Atencion", request.message, "warning");
        });
        $("button[type=submit]").removeAttr("disabled");
}
});


//Evento para generar el reporte en PDF
$(document).on('click',"#genera-pdf" ,function(e){
    e.preventDefault();
    if($("#rango-consulta").val() == ''){
        Swal.fire("Atencion", "Debe escoger una fecha", "warning");
    }
    else{
        let fechas = $("#rango-consulta").val().split("-");
        let datos = {
            'modo'              : $("#tipo-consulta").val(),
            'fecha_inicio'      : invertirFecha(fechas[0].trim()),
            "fecha_fin"         : invertirFecha(fechas[1].trim()),
            'usuario-consulta'  : $("#usuario-consulta").val(),
            'producto-consulta' : $("#producto-consulta").val(),
            'proveedor-consulta': $("#proveedor-consulta").val(),
        }
        let query = btoa(JSON.stringify(datos));
        window.location = "/generareporte/6?q="+query;    
    }
});

$(document).ready(function(){
    $("#detalles").hide();
});


