<?php

namespace App\Controllers;

use App\Models\Almacen_model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Solicitudes_model;
use App\Models\Requerimientos_model;
use App\Models\Productos_model;
use App\Models\Direccion;
class Almacen extends BaseController
{

	use ResponseTrait;

	/*Metodo que carga una tabla con lo que contiene el almacen*/

	public function existencias()
	{
		if ($this->session->get('logged') && $this->session->get('usurol') == 1 || $this->session->get('usurol') == 2 || $this->session->get('usurol') == 3) {
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('almacen/existencias/content');
			echo view('template/footer');
			echo view('almacen/existencias/footer');
		} else {
			return redirect()->to('/');
		}
	}



	//Metodo queo obtiene todas existencias disponibles
	public function listar_existencias($id_categoria=null)
	{
		$model = new Almacen_model();
		$query = $model->obtenerExistencias($id_categoria);
		$existencias = [];
		if (!empty($query)) {
			foreach ($query as $row) {
				$existencia = [
					//PARA PRUEBAS EN LOCAL POR EL DESCODE NO FUNCIONA
					
					// 'prodmodel' => $row->prodmodel,
					// 'itemid' => $row->itemid,
					// 'numexis' => $row->numexis,
					// 'codbar' => $row->codbar,
					// 'prodmar' => $row->prodmar,

					//PARA PRODUCCION
					'prodmodel' => utf8_decode($row->prodmodel),
					'itemid' => $row->itemid,
					'numexis' => $row->numexis,
					'codbar' => $row->codbar,
					'prodmar' => utf8_decode($row->prodmar),
					//Agrega más valores según sea necesario
				];
				$existencias[] = $existencia;
			}
		}
		return $this->response->setJSON($existencias);
	}




	public function reporte_despachos()
	{
		if ($this->session->get('logged') && $this->session->get('usurol') == 1 || $this->session->get('usurol') == 2 || $this->session->get('usurol') == 3) {
			$direccion = new Direccion();
			
			$selectDefault = '<option value="0" selected>Todos</option>';
					$selectDireccion = "";
					$query = $direccion->get_all_data();
					if ($query->resultID->num_rows > 0) {
						$selectDireccion .= $selectDefault;
						foreach ($query->getResult() as $row) {
							$selectDireccion .= '<option value="' . $row->dirid . '">' . utf8_decode($row->dirnom) . '</option>';
						}
					} else {
						$selectDireccion = $selectDefault;
					}
					$tpldata = ["selectDireccion" => $selectDireccion];
			
			
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('almacen/reporte_despachos/content',$tpldata);
			echo view('template/footer');
			echo view('almacen/reporte_despachos/footer');
		} else {
			return redirect()->to('/');
		}
	}

	

//Metodo queo obtiene todas existencias disponibles
public function listar_reporte_despachos($desde=null,$hasta=null,$direccion=null)
{

	
	$model = new Almacen_model();
	
	$query = $model->listar_reporte_despachos($desde,$hasta,$direccion);
	$despachos = [];
	if (!empty($query)) {
		foreach ($query as $row) {
			$existencia = [
				//PARA PRUEBAS EN LOCAL POR EL DESCODE NO FUNCIONA
				
				'numorden' => $row->numorden,
				'statusnom' => $row->statusnom,
				'direccion' => $row->dirnom,
				'ususol' => $row->nombre,
				'fecsal' => $row->fecsalidas,	
			];
			$despachos[] = $existencia;
		}
	}
	return $this->response->setJSON($despachos);
}
	





	//Metodo quee busca si un producto tiene exixtencias 
	public function buscar_producto_existencias($buscar_codbar = null)
	{
		$model = new Almacen_model();
		$query = $model->buscar_producto_existencias($buscar_codbar);
		if (empty($query)) {
			$existencias = [];
		} else {
			$existencias = $query;
		}
		echo json_encode($existencias);
	}


	// public function existencias()
	// {
	// 	if ($this->session->get('logged') && $this->session->get('usurol') == 1 || $this->session->get('usurol') == 2 || $this->session->get('usurol') == 3) {
	// 		$model = new Almacen_model();
	// 		$query = $model->obtenerExistencias();
	// 		$tbody = '';
	// 		if ($query->resultID->num_rows > 0) {
	// 			foreach ($query->getResult() as $row) {
	// 				if ($row->numexis > 0 && $row->numexis < 10) {
	// 					$tbody .= '<tr class="table-warning"><td>' . $row->itemid . '</td><td>' . utf8_decode($row->prodmar) . '</td><td>' . utf8_decode($row->prodmodel) . '</td><td>' . $row->numexis . '</td></tr>';
	// 				} else if ($row->numexis == 0) {
	// 					$tbody .= '<tr class="table-danger"><td>' . $row->itemid . '</td><td>' . utf8_decode($row->prodmar) . '</td><td>' . utf8_decode($row->prodmodel) . '</td><td>' . $row->numexis . '</td></tr>';
	// 				} else {
	// 					$tbody .= '<tr class="table-light"><td>' . $row->itemid . '</td><td>' . utf8_decode($row->prodmar) . '</td><td>' . utf8_decode($row->prodmodel) . '</td><td>' . $row->numexis . '</td></tr>';
	// 				}
	// 			}
	// 		} else {
	// 			$tbody .= '<tr class="table-light"><td colspan="4" class="text-center">Sin Registros</td>';
	// 		}
	// 		echo view('template/header');
	// 		echo view('template/nav_bar');
	// 		echo view('almacen/existencias/content', array('tbody' => $tbody));
	// 		echo view('template/footer');
	// 		echo view('almacen/existencias/footer');
	// 	} else {
	// 		return redirect()->to('/');
	// 	}
	// }


	/*Metodo que obtiene las salidas del almacen*/
	public function salidas()
	{
		if ($this->session->get('logged') && $this->session->get('usurol') == 1 || $this->session->get('usurol') == 2 || $this->session->get('usurol') == 3) {
			$model = new Almacen_model();
			$query = $model->obtenerSalidas();
			$heading = array("Nº de Registro", "Fecha de Salida", "Nº de Orden", "Destino", "Procesado por", "Acciones");
			$rows = array();
			if ($query->resultID->num_rows > 0) {
				foreach ($query->getResult() as $row) {
					$rows[] = array(
						$row->salidaid,
						$this->formatearFecha($row->fecsal),
						$row->numorden,
						utf8_decode($row->depnom),
						utf8_decode($row->usupnom) . ' ' . utf8_decode($row->usupape),
						'<a href="/detalledespacho/' . $row->numorden . '">Detalles</a>'
					);
				}
			} else {
				$rows[] = array('<td colspan="6" class="text-center">Sin Registros</td>', " ", " ", " ", " ", " ");
			}
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('almacen/salidas/content', array('tbody' => $this->generarTabla($heading, $rows)));
			echo view('template/footer');
			echo view('almacen/existencias/footer');
		} else {
			return redirect()->to('/');
		}
	}

	/*Metodo que obtiene las entradas del almacen*/
	public function entradas()
	{
		if ($this->session->get('logged') && $this->session->get('usurol') == 1 || $this->session->get('usurol') == 3) {
			$model = new Almacen_model();
			$query = $model->obtenerEntradas();
			$tbody = '';
			if ($query->resultID->num_rows > 0) {
				foreach ($query->getResult() as $row) {
					$tbody .= '
					<tr>
					<td>' . $row->numregent . '</td>
					<td>' . $row->numfac . '</td>
					<td>' . utf8_decode($row->nomprov) . '</a></td>
					<td>' . $this->formatearFecha($row->fecfac) . '</td>
					<td>' . $this->formatearFecha($row->fecent) . '</td>
					<td>' . utf8_decode($row->usupnom) . ' ' . utf8_decode($row->usupape) . '</td>
					<td>' . utf8_decode($row->entcoment) . '</td>
					<td><a href="/detalleent/' . $row->numregent . '">Detalles</a></td>
					</tr>';
				}
			} else {
				$tbody .= '<tr class="table-light"><td colspan="7" class="text-center">Sin Registros</td>';
			}
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('almacen/entradas/content', array('tbody' => $tbody));
			echo view('template/footer');
			echo view('almacen/entradas/footer');
		} else {
			return redirect()->to('/');
		}
	}

	/*Metodo que refresca la tabla en las existencias*/

	public function refrescarTabla()
	{
		if (
			$this->request->isAJAX() && $this->session->get('logged') && $this->session->get('usurol') == 1
			|| $this->session->get('usurol') == 3
		) {
			$model = new Almacen_model();
			$query = $model->obtenerExistencias();
			$tbody = '';
			if ($query->resultID->num_rows > 0) {
				foreach ($query->getResult() as $row) {
					if ($row->numexis > 5 && $row->numexis < 10) {
						$tbody .= '<tr class="table-warning"><td>' . $row->itemid . '</td><td>' . utf8_decode($row->prodmodel) . '</td><td>' . $row->numexis . '</td></tr>';
					} else if ($row->numexis < 5) {
						$tbody .= '<tr class="table-danger"><td>' . $row->itemid . '</td><td>' . utf8_decode($row->prodmodel) . '</td><td>' . $row->numexis . '</td></tr>';
					} else {
						$tbody .= '<tr class="table-light"><td>' . $row->itemid . '</td><td>' . utf8_decode($row->prodmodel) . '</td><td>' . $row->numexis . '</td></tr>';
					}
				}
			} else {
				$tbody .= '<tr class="table-light"><td colspan="4" class="text-center">Sin Registros</td>';
			}
			return $this->respond(json_encode(array('message' => 'Ejecutado exitosamente', 'tbody' => $tbody)), 200);
		} else {
			return redirect()->to('/');
		}
	}

	/*Metodo que carga la vista para registrar una entrada al almacen*/
	public function registrarEntrada()
	{
		if ($this->session->get('logged') && $this->session->get('usurol') == 1 || $this->session->get('usurol') == 3) {
			$tpldata["tbody"] = $this->cargarExistencias();
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('almacen/entradas/registrar_entrada', $tpldata);
			echo view('template/footer');
			echo view('almacen/entradas/footer');
		} else {
			return redirect()->to('/');
		}
	}
	//Metodo para listar los despachos
	public function despachos()
	{
		$model = new Solicitudes_model();
		if ($this->session->get('logged') && $this->session->get('usurol') == 1 || $this->session->get('usurol') == 3) {
			//Obtenemos las solicitudes aprobadas
			$query = $model->obtenerAprobadas();
			$rows = array();
			$heading = array("N° de orden", "Fecha de aprobacion", "Usuario Solicitante", "Estatus", "Acciones");
			if ($query->resultID->num_rows > 0) {
				foreach ($query->getResult() as $row) {
					if (intval($row->numorden) < 1000000) {
						if ($row->statusid == '2') {
							$rows[] = array(
								$row->numorden,
								$this->formatearFecha($row->fecaprob),
								$row->usupnom . ' ' . $row->usupape,
								$row->statusnom,
								'<a href="/verdespacho/' . $row->numorden . '">Detalles</a>'
							);
						} else {
							$rows[] = array(
								$row->numorden,
								$this->formatearFecha($row->fecaprob),
								$row->usupnom . ' ' . $row->usupape,
								$row->statusnom,
								'<a href="/detalledespacho/' . $row->numorden . '">Detalles</a>'
							);
						}
					}
				}
			} else {
				$rows[] = array('<td colspan="4" class="text-center">Sin Registros</td>', " ", " ", " ");
			}
			//Obtenemos los requerimientos a cargar
			$req_model = new Requerimientos_model();
			$tabla = array();
			$tabheading = array("N° Requerimiento", "Usuario Solicitante", "Fecha de Solicitud", "Fecha de Aprobacion", "Acciones");
			$tabrows = array();
			$lstReq = $req_model->getAllReqAp();
			if ($lstReq->resultID->num_rows > 0) {
				foreach ($lstReq->getResult() as $row) {
					if ($row->statussol == '2') {
						$tabrows[] = array($row->reqid, utf8_decode($row->usupnom) . " " . utf8_decode($row->usupape), $this->formatearFecha($row->fechasol), $this->formatearFecha($row->fecapsol), '<a href="/despachareq/' . $row->reqid . '">Detalles</a>');
					} else {
						$tabrows[] = array($row->reqid, utf8_decode($row->usupnom) . " " . utf8_decode($row->usupape), $this->formatearFecha($row->fechasol), $this->formatearFecha($row->fecapsol), '<a href="/verdespachoreq/' . $row->reqid . '">Detalles</a>');
					}
				}
			} else {
				$tabrows[] = array('<td colspan="5">Sin Registros</td>');
			}
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('almacen/salidas/lista_salidas', array('tbody' => $this->generarTabla($heading, $rows), "tbody2" => $this->generarTabla($tabheading, $tabrows)));
			echo view('template/footer');
			echo view('almacen/salidas/footer');
		} else {
			return redirect()->to('/');
		}
	}

	/*Metodo que registra una nueva entrada al almacen*/
	public function newEntrada()
	{
		if ($this->request->isAJAX() && $this->session->get('logged')) {
			$data = array();
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))), TRUE);
			$model = new Almacen_model();
			$datos['usuregent'] = $this->session->get('userid');
			$datos['numregent'] = intval($model->getLastID()) + 1;
			$query = $model->registrarEntrada($datos);
			if ($query) {
				return $this->respond(array('message' => 'success', 'num_op' => $datos['numregent']), 200);
			} else {
				return $this->respond(array('message' => 'error'), 500);
			}
		} else {
			return redirect()->to('/403');
		}
	}
	/*Metodo para registrar un detalle de la entrada*/
	public function addDetalle()
	{
		if ($this->request->isAJAX() && $this->session->get('logged')) {
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))), TRUE);
			$model = new Almacen_model();
			$query = $model->registrarDetalle($datos);
			if ($query) {
				return $this->respond(array('message' => 'success'), 200);
			} else {
				return $this->respond(array('message' => 'error'), 500);
			}
		} else {
			return redirect()->to('/403');
		}
	}

	/*Metodo para obtener los detalles de la entrada*/
	public function detalleEntrada($id = NULL)
	{

		$model = new Almacen_model();
		$tpldata = array();
		$tbody = '';
		if ($this->session->get('logged')) {
			$detalles = $model->getDetalles($id);
			if ($detalles->resultID->num_rows > 0) {
				foreach ($detalles->getResult() as $row) {
					$tbody .= '<tr>
					<td>' . utf8_encode($row->prodmodel) . '</td>
					<td>' . utf8_encode($row->prodpresent) . '</td>
					<td>' . $row->numunid . '</td>
					<td>' . $row->costuni . '</td>
					</tr>';
				}
			} else {
				$tbody .= '<tr><td colspan="5">Sin Registros<td></tr>';
			}
			$tpldata['tbody'] = $tbody;
			$detFactura = $model->getDetalleEntrada($id);
			if ($detFactura->resultID->num_rows > 0) {
				foreach ($detFactura->getResult() as $row) {
					$tpldata['numregent']  = $row->numregent;
					$tpldata['numfac']     = $row->numfac;
					$tpldata['numrif']     = $row->numrif;
					$tpldata['provnom']    = $row->nomprov;
					$tpldata['fecfac']     = $this->formatearFecha($row->fecfac);
					$tpldata['fechent']     = $this->formatearFecha($row->fecent);
					$tpldata['provdir'] = $row->direccprov;
					$tpldata['provtel1']     = $row->telef1;
					$tpldata['provtel2']     = $row->telef2;
					$tpldata['provemail']      = $row->email;
					$tpldata['usupnom']    = $row->usupnom;
					$tpldata['usupape']    = $row->usupape;
				}
			} else {
				return redirect()->to('/404');
			}

			echo view('template/header');
			echo view('template/nav_bar');
			echo view('almacen/entradas/detalle_entrada', $tpldata);
			echo view('template/footer');
			echo view('almacen/entradas/footer');
		} else {
			return redirect()->to('/');
		}
	}

	//Metodo que refresca la pagina de el detalle del almacen

	public function obtenerDetalles()
	{
		if ($this->request->isAJAX() && $this->session->get('logged')) {
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))), TRUE);
			$model = new Almacen_model();
			$query = $model->getDetalles($datos['numregent']);
			$rows[] = array();
			$headings = array("Código de barras", "Descripcion", "Presentacion", "Costo Unitario", "N° de Unidades");
			if ($query->resultID->num_rows > 0) {
				foreach ($query->getResult() as $row) {
					$rows[] = array(
						trim(utf8_decode($row->codbar)),
						trim(utf8_decode($row->prodmodel)),
						trim(utf8_decode($row->prodpresent)),
						'Bs ' . $row->costuni,
						$row->numunid
					);
				}
			} else {
				$rows[] = array('<td colspan="5" class="text-center">Sin Registros</td>', "", "", "", "");
			}
			return $this->respond(array('message' => 'success', 'data' => $this->generarTabla($headings, $rows)), 200);
		} else {
			return redirect()->to('/');
		}
	}
	//Metodo para cargar la solicitud de despachos

	public function verDespacho($id = NULL)
	{
		$model = new Solicitudes_model();
		$almacenModel = new Almacen_model();
		$tpldata = array();
		$rows = array();
		$headings = array("Descripcion", "Unidades Solicitadas", "Unidades Aprobadas", "Presentacion");
		if ($this->session->get('logged') and $this->session->get('usurol') == 1 or $this->session->get('usurol') == 3) {
			$query = $model->obtenerSalida($id, "2");
			if ($query->resultID->num_rows > 0) {
				foreach ($query->getResult() as $row) {
					$rows[] = array(utf8_decode($row->prodmodel), $almacenModel->obtenerUnidadesSolicitadas($row->numorden, $row->codbar), $row->numuniap, ucwords($almacenModel->obtenerPresentacionProd($row->codbar)));
					$tpldata['deptid'] = $row->deptid;
					$tpldata['usupnom'] = utf8_decode($row->usupnom);
					$tpldata['usupape'] = utf8_decode($row->usupape);
					$tpldata['depnom'] = utf8_decode($row->depnom);
					$tpldata['dirnom'] = utf8_decode($row->dirnom);
					$tpldata['fecaprob'] = $this->formatearFecha($row->fecaprob);
					$tpldata['fecsol'] = $this->formatearFecha($row->fecsol);
					$tpldata['comentario'] = $row->comentario;
				}
			} else {
				return redirect()->to('/404');
			}
			//Se setean los valores para el comprobante
			$tpldata["numorden"] = $id;
			$tpldata["fecsal"] = 'Aún sin despachar';
			$tpldata["commsal"] = 'Aún sin despachar';
			$tpldata["datos"] = $id;
			$tpldata["status"] = "2";
			$tpldata['comentario'] = $row->comentario;
			$tpldata['tbody'] = $this->generarTabla($headings, $rows);
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('almacen/salidas/ver_despacho', $tpldata);
			echo view('template/footer');
			echo view('almacen/salidas/footer');
		} else if ($this->session->get('usurol') == 2) {
			$query = $model->obtenerSalida($id, "2");
			if ($query->resultID->num_rows > 0) {
				foreach ($query->getResult() as $row) {
					$rows[] = array(utf8_decode($row->prodmodel), $row->numuniap, ucwords($row->prodpresent));
					$tpldata['deptid'] = $row->deptid;
					$tpldata['usupnom'] = utf8_decode($row->usupnom);
					$tpldata['usupape'] = utf8_decode($row->usupape);
					$tpldata['depnom'] = utf8_decode($row->depnom);
					$tpldata['dirnom'] = utf8_decode($row->dirnom);
					$tpldata['fecaprob'] = $this->formatearFecha($row->fecaprob);
					$tpldata['fecsol'] = $this->formatearFecha($row->fecsol);
				}
			} else {
				$rows[] = array('<td colspan="3">Sin Registros</td>', '', '');
			}
			//Se setean los valores para el comprobante
			$tpldata["numorden"] = $id;
			$tpldata["fecsal"] = 'Aún sin despachar';
			$tpldata["commsal"] = 'Aún sin despachar';
			$tpldata["datos"] = $id;
			$tpldata["status"] = "2";
			$tpldata['comentario'] = $row->comentario;
			$tpldata['tbody'] = $this->generarTabla($headings, $rows);
			var_dump($tpldata);
			die();
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('almacen/salidas/ver_despacho_admin', $tpldata);
			echo view('template/footer');
			echo view('almacen/salidas/footer');
		} else {
			return redirect()->to('/403');
		}
	}

	//Metodo para registrar la salida del almacen por despacho

	public function registrarSalida()
	{
		$model = new Almacen_model();
		if ($this->request->isAJAX() && $this->session->get('logged')) {
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))), TRUE);
			//Añadimos los datos faltantes para armar el query
			$datos['usureg']   = $this->session->get('userid');
			$datos["fecsal"]   = date("Y-m-d");
			//Insertamos la salida
			$model->nuevoDespacho($datos);
			//cambiamos el estatus de la orden
			unset($model);
			$model = new Solicitudes_model();
			//Cambiamos el estatus de la preorden para que salga en el dashboard
			$model->cambiarStatusPreorden($datos["numorden"], 3);
			//Cambiamos el estatus de la orden
			$query = $model->cambiarStatusOrden($datos["numorden"], 3);
			if ($query) {
				return $this->respond(array("message" => "Despacho registrado exitosamente"), 200);
			} else {
				return $this->respond(array("message" => "Error en la insercion"), 500);
			}
		} else {
			return redirect()->to('/403');
		}
	}

	//Metodo para ver el detalle del despacho

	public function detalleDespacho($id = NULL)
	{
		$model = new Solicitudes_model();
		$almacenModel = new Almacen_model();
		$tpldata = array();
		$rows = array();
		$headings = array("Descripcion", "Unidades Solicitadas", "Unidades Aprobadas", "Presentacion");
		if ($this->session->get('logged') && $id != NULL) {
			$query = $model->obtenerSalida($id, "3");
			if ($query->resultID->num_rows > 0) {
				foreach ($query->getResult() as $row) {
					$rows[] = array(utf8_decode($row->prodmodel), $almacenModel->obtenerUnidadesSolicitadas($row->numorden, $row->codbar), $row->numuniap, ucwords($almacenModel->obtenerPresentacionProd($row->codbar)));
					$tpldata["fecsal"] = $this->formatearFecha($row->fecsal);
					$tpldata["salidaid"] = $row->salidaid;
					$tpldata['commsal'] = urldecode($row->commsal);
					$tpldata['numorden'] = $row->numorden;
					$tpldata['deptid'] = $row->deptid;
					$tpldata['usupnom'] = $row->usupnom;
					$tpldata['usupape'] = $row->usupape;
					$tpldata['depnom'] = $row->depnom;
					$tpldata['dirnom'] = $row->dirnom;
					$tpldata['fecaprob'] = $this->formatearFecha($row->fecaprob);
					$tpldata['fecsol'] = $this->formatearFecha($row->fecsol);
					$tpldata['comentario'] = urldecode($row->comentario);
				}
				$tpldata["tbody"] = $this->generarTabla($headings, $rows);
				$tpldata["datos"] = $id;
				$tpldata["status"] = "3";
				echo view('template/header');
				echo view('template/nav_bar');
				echo view('almacen/salidas/detalle_despacho', $tpldata);
				echo view('template/footer');
				echo view('almacen/salidas/footer');
			} else {
				return redirect()->to('/404');
			}
		} else {
			return redirect()->to('/403');
		}
	}

	//Metodo para cargar una tabla con las existencias
	public function cargarExistencias()
	{
		$model = new Almacen_model();
		$query = $model->obtenerExistencias();
		$rows = array();
		$headings = array("Código Barras", "Marca", "Descripcion", "Numero de unidades", "Acciones");

		if (empty($query)) {
			$rows[] = array('<td colspan="5"> Sin Registros</td>', '<td></td>', '<td></td>', '<td></td>', '<td></td>');
		} else {
			foreach ($query as $row) {
				$rows[] = array($row->codbar, utf8_decode($row->prodmar), utf8_decode($row->prodmodel), $row->numexis, '<button id="' . $row->codbar . '" class="btn btn-sm btn-primary actualizar"><i class="fas fa-plus"></i> Añadir</button>');
			}
		}

		$table = $this->generarTabla($headings, $rows);
		return $table;
	}

	//Metodo para actualizar el catalogo
	public function actualizarCatalogo()
	{
		if ($this->session->get('logged') && $this->request->isAJAX()) {
			$catalog = $this->cargarExistencias();
			return $this->respond(array("message" => "success", "data" => $catalog));
		} else {
			return redirect()->to('/403');
		}
	}

	//Metodo para ver la solicitud de despacho del requerimiento
	public function verSolDesReq($id = NULL)
	{
		$model = new Requerimientos_model();
		$tpldata = array();
		$rows = array();
		$heading = array(" ", "Marca", "Descripcion del Producto", "Unidades Aprobadas", "Codigo de Barras");
		if ($this->session->get('logged') && $this->session->get('usurol') == 1 || $this->session->get('usurol') == 3) {
			//Obtenemos los detalles del solicitante
			$datSol = $model->getDetailsByID('2', $id);
			if ($datSol->resultID->num_rows > 0) {
				foreach ($datSol->getResult() as $row) {
					$tpldata["fechasol"] = $this->formatearFecha($row->fechasol);
					$tpldata["fecapsol"] = $this->formatearFecha($row->fecapsol);
					$tpldata["usupnom"]  = utf8_decode($row->usupnom);
					$tpldata["usupape"]  = utf8_decode($row->usupape);
					$tpldata["dirnom"]   = utf8_decode($row->dirnom);
					$tpldata["depnom"]   = utf8_decode($row->depnom);
					$tpldata["reqid"]    = $row->reqid;
					$tpldata["ususol"]   = $row->ususol;
				}
				//Obtenemos los detalles de la solicitud
				$detSol = $model->getDetailReq($id);
				if ($detSol->resultID->num_rows > 0) {
					foreach ($detSol->getResult() as $row) {
						$rows[] = array(
							'<img style="max-height:4rem;" src="' . base_url() . '/img/' . $row->detimgref . '">',
							utf8_decode($row->detmar),
							utf8_decode($row->detmod),
							$row->detnumuniap,
							'<input class="form-control codbar" type="text" id="' . $row->detid . '" name="' . $row->detid . '">',
						);
					}
				} else {
					$rows[] = array('<td colspan="4">Esta solicitud no tiene registros</td>');
				}
				$tpldata["tbody"] = $this->generarTablaReporte($heading, $rows);
				//Generamos el comentario para la entrada de los productos
				$tpldata["entcomment"] = "Entrada al almacen para el requerimiento N° " . $tpldata["reqid"] . " de fecha " . $tpldata["fechasol"];
				//Usuario que va a registrar la entrada
				$tpldata["usureg"] = $this->session->get('userid');
				//inyectamos los datos en la vista
				echo view('template/header');
				echo view('template/nav_bar');
				echo view('requerimientos/despacho_requerimiento', $tpldata);
				echo view('template/footer');
				echo view('requerimientos/footer');
			} else {
				return redirect()->to('/404');
			}
		} else {
			return redirect()->to('/403');
		}
	}
	//Metodo para registrar el despacho del requerimiento
	public function registrarDespacho()
	{
		$model = new Almacen_model();
		$req_model = new Requerimientos_model();
		$prod_model = new Productos_model();
		$sol_model = new Solicitudes_model();
		//Armamos el arreglo para introducir los detalles de la orden
		$det_ord_sal = array();
		if ($this->session->get('logged') && $this->request->isAJAX() && $this->session->get('usurol') == 1 || $this->session->get('usurol') == 3) {
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))), TRUE);
			//Registramos los productos asociados al requerimiento en la tabla de productos
			foreach ($datos["items"] as $row) {
				//Consultamos los detalles de marca y descripcion en la tabla de detalles del requerimiento
				$det_prod = $req_model->getDetailReqByID($row["itemid"]);
				//Consultamos si el producto esta registrado previamente
				if (!$prod_model->isProductExists($row["codbar"])) {
					//Si no lo esta, lo registramos
					$q1 = $prod_model->newProd(
						[
							"codbar" => $row["codbar"],
							"prodmar" => utf8_encode($det_prod["detmar"]),
							"prodmodel" => utf8_encode($det_prod["detmod"])
						]
					);
				}
				//Poblamos el arreglo para el detalle de las salidas
				$det_ord_sal[] = array(
					"numorden"    => 100000000 + intval($datos["reqid"]),
					"codbar"      => $row["codbar"],
					"numuniap"    => intval($det_prod["detnumunisol"])
				);
				//Registramos el detalle en la tabla de las entradas
				$q2 = $model->registrarDetalle([
					"regent"      => $datos["numregent"],
					"prodpresent" => $row["prodpresent"],
					"numunid"     => $det_prod["detnumunisol"],
					"costuni"     => $row["costuni"],
					"codbar"      => $row["codbar"]
				]);
			}
			//Registramos la orden de salida
			$q3 = $sol_model->newOrden([
				//Al numero de orden le sumamos cien millones para diferenciarlos de las requisiciones 
				"numorden" => 100000000 + intval($datos["reqid"]),
				"fecaprob" => $req_model->getDetailsByID('2', $datos["reqid"])->getRowArray()["fecapsol"],
				"statusid" => 3,
				"usuaprob" => $this->session->get('userid'),
				"ususol"   => $req_model->getDetailsByID('2', $datos["reqid"])->getRowArray()["ususol"]
			]);
			//Insertamos los detalles
			$q4 = $sol_model->insertarDetallesOrden($det_ord_sal);
			//Despues de lo anterior, podremos ahora si, cargar el despacho
			$q5 = $model->nuevoDespacho([
				"fecsal"   => date('Y-m-d'),
				"numorden" => 100000000 + intval($datos["reqid"]),
				"depdest"  => $req_model->getDetailsByID('2', $datos["reqid"])->getRowArray()["depsol"],
				"usureg"   => $this->session->get('userid'),
				"commsal"  => $datos["commsal"]
			]);
			//Recorremos nuevamente el arreglo para actualizar las existencias
			$q = '';
			foreach ($datos["items"] as $row) {
				$q = $model->actualizaExistencias($row["codbar"], intval($req_model->getDetailReqByID($row["itemid"])["detnumunisol"]), 2);
			}
			//Si todo esta bien, le cambiamos el estatus a despachado al requerimiento
			if ($q && $req_model->changeStatus("2", "3", $datos["reqid"])) {
				return $this->respond(["message" => "Despacho cargado exitosamente"], 200);
			} else {
				return $this->respond(["message" => "error en la insercion"], 500);
			}
		}
	}
}
