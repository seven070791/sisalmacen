<meta charset="utf-8">
<div class="content-wrapper">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css/reportes.css">
  <style>
    table.dataTable thead,
    table.dataTable tfoot {
      background: linear-gradient(to right, #a9b6c2, #a9b6c2, #a9b6c2);
    }
  </style>
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12 p-2">

        </div>
      </div>
      <!--Form-->

      <div class="row">

        <div class="col-lg-12 col-sm-12 col-md-12 ">
          <div class="card">
          <form role="form" method="POST" id="filtrar">
              <div class="card-body">
                <div class="form-group">
                <label for="categoria">Categoria</label>
                <select class="control" name="categoria" id="categoria" >
                <option value="0" disabled>Seleccione</option>
                </select>
                <button type="submit" class="btn-sm btn-primary">filtrar</button>
              </div>
             
          </form>
          <div class="row">
            <div class="col-1">
            </div>
            <div class="col-10 ">
              <table class="display" id="table_existencias" style="width:100%" style="margin-top: 20px">
                <thead>
                  <tr>
                    <th style="width:11%">Codigo</th>
                    <th style="width:11%">ID Item</th>
                    <th>Marca</th>
                    <th style="width:40%">Producto</th>
                    <th>Cantidad</th>
                  </tr>
                </thead>
                <tbody id="listar_casos">
                </tbody>
              </table>
            </div>
          </div>
          </div>
          </div>
        </div>
      </div>
    </div>

    <!-- /.card-body -->
    <!-- <div class="card-footer">
      <div class="row">
        <div class="col-1">
          Leyenda:
        </div>
        <div class="col-2">
          <span class="badge badge-warning">Pocas Unidades</span>
        </div>
        <div class="col-2">
          <span class="badge badge-danger">Agotado</span>
        </div>
      </div>
    </div> -->
  </div>
  <!-- /.card -->

  </section>

  <!-- <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
  <script>
    $.extend($.fn.dataTable.defaults, {
      language: {
        url: '//cdn.datatables.net/plug-ins/1.10.24/i18n/Spanish.json'
      },
      // Otros ajustes del DataTable
    });
  </script> -->
</div>