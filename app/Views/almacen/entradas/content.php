<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css/reportes.css">


  <style>
    table.dataTable thead,
    table.dataTable tfoot {
      background: linear-gradient(to right, #a9b6c2, #a9b6c2, #a9b6c2);
    }
  </style>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Historico de entradas de productos</h3>

        <div class="card-tools">
          <a class="btn btn-sm btn-primary" href="/regentrada">Añadir</a>
        </div>
      </div>
      <div class="card-body">
        <table class="table table-hover table-light text-center tabla">
          <thead>
            <tr>
              <td>Nº Registro</td>
              <td>Nº Factura</td>
              <td>Proveedor</td>
              <td>Fecha Factura</td>
              <td>Fecha Operacion</td>
              <td>Usuario</td>
              <td>Comentario</td>
              <td></td>
            </tr>
          </thead>
          <tbody id="entradas">
            <?php echo $tbody; ?>
          </tbody>
        </table>
      </div>
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->