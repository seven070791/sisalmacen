<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type">
  <meta content="text/html">
  <meta charset="utf-8">
  <meta lang="es">
  <title>Intranet SAPI - Caso Remitido</title>
  <style type="text/css">
    body {
      padding-top: 0 !important;
      padding-bottom: 0 !important;
      padding-top: 0 !important;
      padding-bottom: 0 !important;
      margin: 0 !important;
      width: 100% !important;
      -webkit-text-size-adjust: 100% !important;
      -ms-text-size-adjust: 100% !important;
      -webkit-font-smoothing: antialiased !important;
    }

    .tableContent img {
      border: 0 !important;
      display: block !important;
      outline: none !important;
    }

    a {
      color: #382F2E;
    }

    p,
    h1 {
      color: #382F2E;
      margin: 0;
    }

    p {
      text-align: left;
      color: #999999;
      font-size: 14px;
      font-weight: normal;
      line-height: 19px;
    }

    a.link1 {
      color: #382F2E;
    }

    a.link2 {
      font-size: 16px;
      text-decoration: none;
      color: #ffffff;
    }

    h2 {
      text-align: left;
      color: #222222;
      font-size: 19px;
      font-weight: normal;
    }

    div,
    p,
    ul,
    h1 {
      margin: 0;
    }

    .bgBody {
      background: #ffffff;
    }

    .bgItem {
      background: #ffffff;
    }

    @media only screen and (max-width:480px) {

      table[class="MainContainer"],
      td[class="cell"] {
        width: 100% !important;
        height: auto !important;
      }

      td[class="specbundle"] {
        width: 100% !important;
        float: left !important;
        font-size: 13px !important;
        line-height: 17px !important;
        display: block !important;
        padding-bottom: 15px !important;
      }

      td[class="spechide"] {
        display: none !important;
      }

      img[class="banner"] {
        width: 100% !important;
        height: auto !important;
      }

      td[class="left_pad"] {
        padding-left: 15px !important;
        padding-right: 15px !important;
      }

    }

    @media only screen and (max-width:540px) {

      table[class="MainContainer"],
      td[class="cell"] {
        width: 100% !important;
        height: auto !important;
      }

      td[class="specbundle"] {
        width: 100% !important;
        float: left !important;
        font-size: 13px !important;
        line-height: 17px !important;
        display: block !important;
        padding-bottom: 15px !important;
      }

      td[class="spechide"] {
        display: none !important;
      }

      img[class="banner"] {
        width: 100% !important;
        height: auto !important;
      }

      .font {
        font-size: 18px !important;
        line-height: 22px !important;

      }

      .font1 {
        font-size: 18px !important;
        line-height: 22px !important;

      }
    }
  </style>
  <script type="colorScheme" class="swatch active">
    {
            "name":"Default",
            "bgBody":"ffffff",
            "link":"382F2E",
            "color":"999999",
            "bgItem":"ffffff",
            "title":"222222"
            }
    </script>
</head>
<style>
.card {
  background: linear-gradient(to right, #fafbfc, #f3f5f7, #a9b6c2);
  
  border-radius: 10px;
  padding: 20px;

  text-align: center;
}
</style>
<body paddingwidth="0" paddingheight="0" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
  <table bgcolor="#ffffff" width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent" align="center" style="font-family:Helvetica, Arial,serif;">
    <tbody>
      <tr>
        <td>
          <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" class="MainContainer">
            <tbody>
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody>
                      <tr>
                        <td valign="top" width="40">&nbsp;</td>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                              <!-- =============================== Header ====================================== -->
                              <tr>
                                <td height="75" class="spechide"></td>
                                <!-- =============================== Body ====================================== -->
                              </tr>
                              <tr>
                                <td class="movableContentContainer" valign="top">
                                  <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <tbody>
                                        <tr>
                                          <td height="35"></td>
                                        </tr>
                                        <tr>
                                          <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tbody>
                                                <tr>
                                                  <td valign="top" align="center" class="specbundle">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                      <div class="contentEditable">

                                                      </div>
                                                      <p style="text-align:center;margin:0;font-family:Georgia,Time,sans-serif;font-size:26px;color:#222222;"><span class="specbundle2"><span class="font1"></span></span></p>
                                                    </div>
                                                  </td>
                                                  <td valign="top" class="specbundle">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                      <div class="contentEditable">
                                                      </div>
                                                    </div>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <div class="card">
                                  <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                     
                                      <tr>
                                        <td valign="top" align="center">
                                          <div class="contentEditableContainer contentImageEditable">
                                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <td>
                                            <img src="https://atencion.sapi.gob.ve/logo_sapi_2020.png" width="120" height="80" alt="Logo del SAPI" data-default="placeholder" data-max-width="560">
                                            
                                            </td>
                                            <td>
                                            <img src="https://atencion.sapi.gob.ve/LogoSIAC_sapi2.png" width="120" height="80" alt="Logo del SAPI" data-default="placeholder" data-max-width="560">
                                            </td>
                                          </table>
                                            
                                          </div>
                                        </td>
                                      </tr>
                                    </table>
                                  </div>
                                  <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                      <tr>
                                        <td height="55"></td>
                                      </tr>
                                      <tr>
                                        <td align="left">
                                          <div class="contentEditableContainer contentTextEditable">
                                           
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="15"></td>
                                      </tr>
                                      <tr>

                                        <td align="left">
                                          <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable" align="center">

                                        
                                            <td align="left">
                                          <div class="contentEditableContainer contentTextEditable">
                                            <div class="contentEditable" align="center">

                                              <b>
                                                <p style=" color:#222222;">Hola, <?php echo $name; ?>
                                              </b>
                                              <br>
                                              <br>
                                              <p style=" color:#222222;">Hemos recibido una solicitud de recuperacion de contrase&#241;a
                                                Para cambiar tu contrase&#241;a haz click en el siguiente enlase :
                                                <a href="<?php echo base_url(); ?>/confirmRecover/<?php echo $urldata; ?>
                                                  <b style=" color:#222222;">
                                                  </b>
                                                  <p style=" color:#222222;">Direccion de Sistemas y Tecnologias de la Informacion: Soporte Tecnico de SISALMACEN del Servicio Autonomo de la Propiedad Intelectual
                                                  </p>
                                                </a>
                                                <br>
                                              <p style=" color:#222222;"> Saludos cordiales
                                                <br>
                                            </div>
                                          </div>
                                        </td>
                                                
                                            </div>
                                          </div>
                                          </div>

                                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                              <tbody>
                                                <tr>
                                                  <td valign="top" class="specbundle">
                                                    <div class="contentEditableContainer contentTextEditable">
                                                      <div class="contentEditable" align="center">
                                                        <p style=" color:#222222; font-size: 11px;"> 
                                                          <span >Servicio Autonomo de la Propiedad Intelectual</span>
                                                          <br>
                                                          Centro Sim&#243;n Bol&#237;var,
                                                          Edificio Norte, Piso 4, El Silencio, al lado de la Plaza Caracas.
                                                          <br>
                                                          Telefonos: 0212-484.97.61
                                                          0212-484.64.78 0212-484.29.07
                                                          <br>
                                                        </p>
                                                      </div>
                                                    </div>
                                                  </td>
                                                  <td valign="top" width="30" class="specbundle">&nbsp;</td>
                                                  <td valign="top" class="specbundle">

                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>

                                        </td>
                                      </tr>
                                      <tr>
                                        <td height="55"></td>
                                      </tr>
                                      <tr>
                                        <td height="20"></td>
                                      </tr>
                                    </table>
                                  </div>
                                  
                                  </div>
                                  <!-- =============================== footer ====================================== -->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                        <td valign="top" width="40">&nbsp;</td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</body>

</html>