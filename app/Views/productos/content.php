  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/css/reportes.css">
    <script src="custom/js/excel.js"></script>
    <style>
      table.dataTable thead,
      table.dataTable tfoot {
        background: linear-gradient(to right, #a9b6c2, #a9b6c2, #a9b6c2);
      }
    </style>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Listado de productos cargados</h3>

          
        </div>

        <div class="card-body">
        <div class="row"> 
            <div class="col-1">
            </div>
            <div class="card-tools">
            <a href="/regprod" class="btn btn-sm btn-primary">Añadir</a>
          </div>&nbsp;
          <button id="sheetjsexport" class="btn btn-sm small-button">Excel</button>
        </div>
          <br>
          <div class="row"> 
            <div class="col-1">
            </div>
            
            <div class="col-10">
              <table class="table  table-light tabla" id="listadoprod">
                <thead>
                  <tr>
                    <td>Código</td>
                    <td>Marca</td>
                    <td>Descripcion</td>
                    <td>Categoria</td>
                    <td>Estatus</td>
                    <td></td>
                  </tr>
                </thead>
                <tbody>
                  <?php echo $tbody; ?>
                </tbody>
              </table>
            </div>

          </div>
         
        </div>
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
  </div>



  <script>
    document.getElementById("sheetjsexport").addEventListener('click', function() {
      /* Create worksheet from HTML DOM TABLE */
      var wb = XLSX.utils.table_to_book(document.getElementById("listadoprod"));
      /* Export to file (start a download) */
      XLSX.writeFile(wb, "Listado_Productos.xlsx");
    });
  </script>
  <!-- /.content-wrapper -->